package com.example.imcjava;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btnCalcular, btnLimpiar, btnCerrar;
    private EditText txtAltura, txtPeso, txtIMC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        txtAltura = (EditText) findViewById(R.id.txtAltura);
        txtPeso = (EditText) findViewById(R.id.txtPeso);
        txtIMC = (EditText) findViewById(R.id.txtIMC);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtAltura.getText().toString().matches("") || txtPeso.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this,
                            "Falta ingresar datos",
                            Toast.LENGTH_SHORT).show();
                } else {
                    calcularIMC();
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtAltura.setText("");
                txtPeso.setText("");
                txtIMC.setText("");
                txtAltura.requestFocus();
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
        private void calcularIMC() {
            float peso = Float.parseFloat(txtPeso.getText().toString());
            float altura = Float.parseFloat(txtAltura.getText().toString());

            float imc = peso / (altura * altura);

            String imcResultado = String.format("%.2f", imc);
            txtIMC.setText(imcResultado);
        }
}